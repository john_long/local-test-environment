# Local Test Environment

## Requirements
- Install Vagrant
- Install VirtualBox

### List VM's

A list of the virtual machines that can be created can be found by running the following from the projects root directory:

```bash
vagrant status
```

### Bring up/create one of the defined virtual machines

A single virtual machine can be brought up by running the following inside the projects root directory:

```bash
# Get a list of virtual machines
vagrant status

# Bring up a single virtual machine using one defined in the list from the above command
vagrant up <machine_name>
```

Bring up all defined virtual machines:

```bash
vagrant up
```

### Destroy virtual machines

Destroy a single virtual machine:

```bash
# Get a list of virtual machines
vagrant status

# Destroy a single virtual machine using one defined in the list from the above command
# Use the '-f' flag for force if the machine is still running
vagrant destroy <machine_name>
```

### Environment Variables

To specify the OS image for the `gitlab-omnibus` machine run the following command from the projects root directory:

```bash
OMNIBUS_IMAGE='centos/7' vagrant up gitlab-omnibus
```

To specify the OS image for the `gitlab-runner` machine run the following command from the projects root directory:

```bash
RUNNER_IMAGE='ubuntu/bionic64' vagrant up gitlab-runner
```

To specify the OS image for the `os-only` machine run the following command from the projects root directory:

```bash
OS_IMAGE='centos/7' vagrant up os-only
```

To specify the version of GitLab Omnibus run the following command from the projects root directory:

```bash
OMNIBUS_IMAGE='centos/7' GL_VERSION='13.10.1' vagrant up gitlab-omnibus
```
